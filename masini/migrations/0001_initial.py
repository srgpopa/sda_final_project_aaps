# Generated by Django 3.2 on 2021-04-14 15:15

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Masina',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('numar_inmatriculare', models.CharField(max_length=9)),
                ('nume_sofer', models.CharField(max_length=255)),
                ('marca', models.CharField(max_length=255)),
                ('model', models.CharField(max_length=255)),
                ('categorie', models.CharField(max_length=255)),
                ('utilizare', models.CharField(max_length=255)),
                ('tip_combustibil', models.CharField(max_length=255)),
                ('an_fabricatie', models.DateField()),
                ('culoare', models.CharField(max_length=255)),
                ('km_bord', models.IntegerField()),
                ('nr_talon', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='NumeSpecificatie',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nume', models.CharField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='MasinaNumeValoareSpecificatie',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('val', models.CharField(max_length=255)),
                ('masina', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='masini.masina')),
                ('nume_specificatie', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='masini.numespecificatie')),
            ],
        ),
        migrations.CreateModel(
            name='Alerta',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('titlu', models.CharField(max_length=255)),
                ('continut', models.TextField()),
                ('cost', models.PositiveIntegerField(default=0)),
                ('masina', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='masini.masina')),
            ],
        ),
    ]
