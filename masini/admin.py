from django.contrib import admin

# Register your models here.
from masini.models import Masina, NumeSpecificatie

admin.site.register(Masina)
admin.site.register(NumeSpecificatie)