from django.urls import path, include

from masini.views import MasiniListView, MasiniUpdateView, MasiniCreateView, AlerteCreateView, MasiniDeleteView, \
    SpecificatiiListView

urlpatterns = [
    path('', MasiniListView.as_view(), name='masini_list'),
    path('masini-update/<int:pk>/', MasiniUpdateView.as_view(), name='masini_update'),
    path('masini-create/', MasiniCreateView.as_view(), name='masini_create'),
    path('masini-delete/<int:pk>/', MasiniDeleteView.as_view(), name='masini_delete'),

    path('specificatie-list/', SpecificatiiListView.as_view(), name='specificatie_list'),

    path('alerte-create/', AlerteCreateView.as_view(), name='alerte_create'),
]
