
from django.db import models


class Masina(models.Model):
    numar_inmatriculare = models.CharField(max_length=9)
    nume_sofer = models.CharField(max_length=255)
    marca = models.CharField(max_length=255)
    model = models.CharField(max_length=255)
    categorie = models.CharField(max_length=255)
    utilizare = models.CharField(max_length=255)
    tip_combustibil = models.CharField(max_length=255)
    an_fabricatie = models.DateField()
    culoare = models.CharField(max_length=255)
    km_bord = models.IntegerField()
    nr_talon = models.IntegerField()

    def __str__(self):
        return f'{self.marca}-{self.model}'


class NumeSpecificatie(models.Model):
    nume = models.CharField(max_length=255)


class MasinaNumeValoareSpecificatie(models.Model):
    masina = models.ForeignKey(Masina, on_delete=models.CASCADE)
    nume_specificatie = models.ForeignKey(NumeSpecificatie, on_delete=models.DO_NOTHING)
    val = models.CharField(max_length=255)


class Alerta(models.Model):
    titlu = models.CharField(max_length=255)
    continut = models.TextField()
    cost = models.PositiveIntegerField(default=0)
    masina = models.ForeignKey(Masina, on_delete=models.CASCADE)

