from django.urls import reverse_lazy
from django.views.generic import ListView, UpdateView, CreateView, DeleteView
#
from masini.models import Masina, Alerta, NumeSpecificatie


class MasiniListView(ListView):
    template_name = 'masini/masini-list.html'
    model = Masina
    context_object_name = 'toate_masinile'


class MasiniDeleteView(DeleteView):
    template_name = 'masini/masini-delete.html'
    model = Masina
    success_url = reverse_lazy('masini_list')


class MasiniUpdateView(UpdateView):
    template_name = 'masini/masini-update.html'
    model = Masina
    fields = '__all__'
    success_url = reverse_lazy('masini-list')


class MasiniCreateView(CreateView):
    template_name = 'masini/masini-create.html'
    model = Masina
    fields = '__all__'
    success_url = reverse_lazy('masini_list')


class SpecificatiiListView(ListView):
    template_name = 'numespecificatii/specificatii-list.html'
    model = NumeSpecificatie
    context_object_name = 'toate_specif'


class AlerteListView(ListView):
    template_name = 'alerte/alerte-list.html'
    model = Alerta
    context_object_name = 'toate_alertele'


class AlerteCreateView(CreateView):
    template_name = 'alerte/alerte-create.html'
    model = Alerta
    fields = '__all__'
    success_url = reverse_lazy('alerte-list')
